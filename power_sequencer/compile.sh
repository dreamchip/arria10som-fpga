#!/bin/sh
# execute in nios command shell: ${QUARTUS_DIR}/nios2eds/nios2_command_shell.sh

# Generate flash initialization file and copy to project folder
cd hex_creator_script
python3 ./create_flash_hex.py
cp power_sequencer_qsys_onchip_flash.hex ../
cd ..

# Compile revision for customer
quartus_cmd power_sequencer -c power_sequencer_customer
quartus_cdb power_sequencer -c power_sequencer_customer --update_mif
quartus_asm --read_settings_files=off --write_settings_files=off power_sequencer -c power_sequencer_customer



