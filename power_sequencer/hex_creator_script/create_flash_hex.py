#-----------------------------------------------------------------------------
# $HeadURL$
# $Author$
# $Rev$
# $Date$
#-----------------------------------------------------------------------------
# This is an unpublished work, the copyright in which vests in
# DreamChip Technologies GmbH. The information contained herein is the property
# of DreamChip Technologies GmbH and is supplied without liability for errors or
# omissions. No part may be reproduced or used except as authorized by
# contract or other written permission.
# Copyright(c) DreamChip Technologies GmbH, 2016-2017. All rights reserved.
#-----------------------------------------------------------------------------
# Module    : Power Sequencer
# Hierarchy : Wiesmann
# Purpose   : Create an Intel Hex-File with initial clock generator configuration and ADC threshold values
# Creator   : vahlsingj
# Remark    : v0.1
#-----------------------------------------------------------------------------

#TODO:
# - Fix:  - CRC32 algorithm returns hex value with lower case letters

import io
import math
import zlib, binascii
import sys

# in- and output files
ADC_THRESHOLD_FILE_PATH = 'adc_threshold_values.txt'
A10_CONF_FILE_PATH      = 'a10_configuration.txt'
CLOCK_GEN_HEADER_PATH   = 'Clkgen-Registers.h' # 'Si5341-RevD-DCTWM001-Registers.h'
HEXFILE_PATH            = 'power_sequencer_qsys_onchip_flash.hex'



A10_CONF_OFFSET = 0                     # start of UFM Sector 1
A10_CONF_MAX_DATA_BYTE_CHUNK_SIZE = 8

CLOCK_GEN_DATA_OFFSET = 6               # 2 bytes Arria10 configuration data + 4 bytes CRC-32
CLOCK_GEN_MAX_DATA_BYTE_CHUNK_SIZE = 16 # 0 < CLOCK_GEN_MAX_DATA_BYTE_CHUNK_SIZE < 255

ADC_THRESH_DATA_OFFSET = 16384          # start of UFM Sector 2  
ADC_MAX_ACTIVE_CHANNELS = 8
ADC_THRESH_MAX_DATA_BYTE_CHUNK_SIZE = 4
ADC_MAX_UPPER_THRESH = 4095
ADC_MIN_LOWER_THRESH = 0



CRC32_GENERATOR_POLY = '04C11DB7'


# some helping functions
form_hex = lambda x, n: "{:0{}X}".format(x % (16**2)**n, n*2)

def split_data(data, chunk_size) :
    """Split an iterable into chunks of the same size.
    Depending on the size of 'data' and the value of 'chunk_size' the last chunk might
    be smaller than 'chunk_size'"""
    print('Split data into chunks', end='')
    n = math.ceil(len(data) / chunk_size)
    chunks = [data[i*chunk_size:(i+1)*chunk_size] for i in range(n)]
    print(' -> DONE')
    return chunks

def calc_crc32(data_hex, hex_gen_poly=CRC32_GENERATOR_POLY, bin_gen_poly=None) :
    """Calculate a simple CRC-32 checksum using the generator polynom 0x04C11DB7."""
    data_bin = bin(int(data_hex, 16))[2:]
    data_bin = data_bin.zfill(4*len(data_hex))
    if bin_gen_poly == None :
        bin_gen_poly = bin(int(hex_gen_poly, 16))[2:]
        bin_gen_poly = bin_gen_poly.zfill(4*len(hex_gen_poly))

    checksum = 32 * '0'
    for bit in data_bin :
        checksum0 = checksum[0]       # 
        checksum = checksum[1:] + '0' # leftshift
        if (checksum0 != bit) :       # xor with generator polynom
            tmp = ''
            for j in range(32) :
                if (checksum[j] == bin_gen_poly[j]) :
                    tmp += '0'
                else :
                    tmp += '1'
            checksum = tmp
    checksum = hex(int(checksum, 2))[2:].zfill(8)

    return checksum


def read_adc_conf(from_file=True, from_stdin=False) :
    """Parse either a .txt file or get the ADC thresholds from stdin(not implemented yet)
    and return a hex string containing the ADC threshold data"""
    thresh_data = []

    if from_file :
        print('Reading threshold file', end='')
        file = open(ADC_THRESHOLD_FILE_PATH, 'r')
        lines = file.readlines()
        file.close()


        print(' -> DONE')
        print('Parsing threshold file', end='')
        lines = [x.strip() for x in lines]
        rel_lines = [x.split(',') for x in lines if not (x.startswith('#') or len(x)==0)]
        rel_lines = [[y.strip() for y in x] for x in rel_lines if len(x) == 3]

        rel_lines = [[int(y) for y in x] for x in rel_lines if x[0].isdecimal() and x[1].isdecimal() and x[2].isdecimal()]
        rel_lines = [x for x in rel_lines if ADC_MIN_LOWER_THRESH <= x[1] <= ADC_MAX_UPPER_THRESH and x[2] in (0, 1)]
        
        data = [form_hex(x[0]*2**12+x[1], 2) for x in rel_lines]
        for i in range(1, ADC_MAX_ACTIVE_CHANNELS+1) :
            lower = upper = -1
            for j in range(len(rel_lines)) :
                if rel_lines[j][0] == i :
                    if rel_lines[j][2] == 0 :
                        if lower == -1 :
                            lower = data[j]
                    elif rel_lines[j][2] == 1 :
                        if upper == -1 :
                            upper = data[j]
                if upper != -1 and lower != -1 :
                    break
            if upper == -1 :
                upper = "{}FFF".format(i)
            if lower == -1 :
                lower = "{}FFF".format(i)
            thresh_data.append(upper+lower)
        print(' -> DONE')
    elif from_stdin:
        print('Not implemented yet!') 
        
    return ''.join(thresh_data)
        
def read_clock_gen_conf(from_header=True, from_csv=False) :
    """Parse either a C header file or a CSV file created by Clock Generator Pro Software
    and return a hex string containing the configuration data"""
    if from_header :
        print('Reading header file', end='')
        # read header file
        headerfile = open(CLOCK_GEN_HEADER_PATH, 'r', encoding='iso8859-1')
        headerlines = headerfile.readlines()
        headerfile.close()
        print(' -> DONE')
        print('Parsing header file', end='')
        # parse content of header file
        rel_headerlines = [x.strip() for x in headerlines if  x.startswith('\t{') or x.startswith('#define')]
        conf_data = [x[1:-2].strip().split(' ') for x in rel_headerlines if x.startswith('{') and x.endswith('},')]
        conf_data = [[x.replace('0x', '').replace(',', '') for x in data] for data in conf_data]
        conf_data = [''.join(x) for x in conf_data]
        conf_data = ''.join(conf_data)
        print(' -> DONE')
    elif from_csv :
        print('Not implemented yet!')        
    return conf_data
    
def read_a10_conf(from_file=True, from_stdin=False) :
    """Parse either a .txt file or get the Arria10 configuration from stdin(not implemented yet)
    and return a hex string containing the Arria10 configuration data"""
    conf_data = ["", ""]

    if from_file :
        print('Reading configuration file', end='')
        file  = open(A10_CONF_FILE_PATH, 'r')
        lines = file.readlines()
        file.close()
        
        print(' -> DONE')
        print('Parsing configuration file', end='')
        # ignore all lines that don't match the pattern 'x = y', where x has to be a paramter name and y 0 or 1
        lines = [x.strip() for x in lines]
        rel_lines = [x.split('=') for x in lines if not x.startswith('#')]      # ignore comments
        rel_lines = [[y.strip() for y in x] for x in rel_lines if len(x) == 2]  # look for pattern
        #rel_lines = [x for x in rel_lines if x[1] == '0' or x[1] == '1']        # continued

        # now look for both known parameter names a10_msel1_ctl and a10_hps_bsel
        for line in rel_lines :
            if conf_data[0] == "" and line[0].lower() == "a10_msel1_ctl":
                conf_data[0] = form_hex(int(line[1], 2), 1)
                print("\na10_msel1_ctl: {}".format(conf_data[0]))
            elif conf_data[1] == "" and line[0].lower() == "a10_hps_bsel":
                conf_data[1] = form_hex(int(line[1]), 1)
                print("a10_hps_bsel: {}".format(conf_data[1]))
            elif conf_data[0] != "" and conf_data[1] != "" :
                break
        print(' -> DONE')
    elif from_stdin:
        print('Not implemented yet!')        
    
    return ''.join(conf_data)

def data_append_crc(conf_data) :
    """Generate and append a CRC checksum to the conf_data string.
    At the moment there is only one CRC algorithm implemented."""
    print("Generate and append CRC-32", end='')
    conf_data += calc_crc32(conf_data)
    print(" -> DONE")
    return conf_data

def calc_intel_hex_checksum(data_bytes) :
    """Calculate the mod(256) checksum each record has to end with, according to the Intel-Hex standard."""
    decimal_data = [int('0x'+data_bytes[i:i+2], 16) for i in range (0, len(data_bytes), 2)]
    return form_hex(-sum(decimal_data), 1)

def hex_string_to_intel_hex_records(data, offset, chunk_size) :
    """Convert a hex string to Intel Hex file records with 'chunk_size' bytes of data each.
    Start address is 'offset'."""
    records = []

    # split data string into chunks of size 2*chunk_size
    # 2 times because the string consists of nibbles and we want to store in multiples of bytes
    record_data_bytes = split_data(data, 2*chunk_size)
    n = len(record_data_bytes)

    # store hex chunk size for each chunk
    # the first 'n'-1 are of size 'chunk_size', the last one might be less or equal 'chunk_size'
    print('Store data chunk sizes', end='')
    chunk_size_hex = form_hex(chunk_size, 1)
    last_chunk_size_hex = form_hex(int(len(record_data_bytes[-1])/2), 1)
    record_chunk_sizes_hex = [chunk_size_hex for i in range(n-1)]
    record_chunk_sizes_hex.append(last_chunk_size_hex)
    print(' -> DONE')

    # calculate the corresponding addresses
    print('Calculate data chunk addresses', end='')
    record_chunk_addresses = [form_hex(i * chunk_size + offset, 2) for i in range(n)]
    print(' -> DONE')

    # the next
    print('Create records containing data bytes')
    for i in range(0, n) :
        record = ':'
        record += record_chunk_sizes_hex[i]                         # data length
        record += record_chunk_addresses[i]                         # address
        record += '00'                                              # record type
        record += record_data_bytes[i]                              # data bytes
        record += calc_intel_hex_checksum(record[1:]) + '\n'        # checksum
        records.append(record)
        print('\t' + str(record.encode('ascii')))
    print('\t-> DONE')

    return records

def intel_hex_eof_record() :
    """Returns the EOF file record every Intel Hex file ends with."""
    print('Create EOF record', end='')
    record_eof = ':00000001FF'
    print(' -> DONE')
    return record_eof                                               # EOF

def get_adc_threshold_records() :
    """Returns Intel Hex records containing the ADC threshold values."""
    thresh_data = read_adc_conf()
    thresh_data = data_append_crc(thresh_data)

    print('Create data byte records')
    records = hex_string_to_intel_hex_records(thresh_data, ADC_THRESH_DATA_OFFSET, ADC_THRESH_MAX_DATA_BYTE_CHUNK_SIZE)
    print(' -> DONE')

    return records

def get_clock_gen_data_records() :
    """Returns Intel Hex records containing the clock generator's configuration data."""
    conf_data = read_clock_gen_conf()
    conf_data = data_append_crc(conf_data)

    # first 2 bytes contain the total number of data bytes
    print('Create record containing number of data bytes')
    record_nodb = hex_string_to_intel_hex_records(form_hex(int(len(conf_data)/2), 2), CLOCK_GEN_DATA_OFFSET, 2)
    records = record_nodb[:]
    print(' -> DONE')
    
    print('Create data byte records')
    records += hex_string_to_intel_hex_records(conf_data, CLOCK_GEN_DATA_OFFSET+2, CLOCK_GEN_MAX_DATA_BYTE_CHUNK_SIZE)
    print(' -> DONE')

    return records

def get_a10_conf_records() :
    """Returns Intel Hex records containing the clock generator's configuration data."""
    conf_data = read_a10_conf()
    conf_data = data_append_crc(conf_data)

    print('Create data byte records')
    records = hex_string_to_intel_hex_records(conf_data, A10_CONF_OFFSET, A10_CONF_MAX_DATA_BYTE_CHUNK_SIZE)
    print(' -> DONE')

    return records


# main part of the program
if __name__ == "__main__" :
    records = []
 
    print("Arria10 configuration")
    records += get_a10_conf_records()
    print("\n\nClock Gen configuration")
    records += get_clock_gen_data_records()
    print("\n\nADC Thresholds")
    records += get_adc_threshold_records()
    records.append(intel_hex_eof_record())

    print("Write records to file '%s'" % HEXFILE_PATH, end='')
    hexfile = open(HEXFILE_PATH, 'w')
    hexfile.writelines(records)
    hexfile.close()
    print(' -> DONE')

    print('Done converting')

#    print(last_chunk_size_hex)



    
