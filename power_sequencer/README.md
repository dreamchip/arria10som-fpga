# Custom Board Management Controller (power_sequencer_customer)

## Contents

| File                                           | Description
| ---------------------------------------------- | -------------------------------------------------------
| compile.sh                                     | Creates a new .pof file for the Power Sequencer.
| hex_creator_script/a10_configuration.txt       | Config file for MSEL and BSEL values
| hex_creator_script/adc_threshold_values.txt    | Thresholds for power sequencing, leave as is.
| hex_creator_script/Clkgen-Project.slabtimeproj | Exported C-header file for Silabs clock generator 
| hex_creator_script/Clkgen-Registers.h          | Exported C-header file for Silabs clock generator 
| hex_creator_script/create_flash_hex.py         | Config to flash init conversion script
| nios.hex                                       | Nios software
| power_sequencer_customer.qsf                   | Quartus setting file.
| power_sequencer.qpf                            | Quartus project file.
| power_sequencer_top.qxp                        | Post fitting netlist of design.

The SOM features a Max 10 Board Management Controller that is responsible for the MSEL/BSEL settings as well as the startup configuration of the clock generator on the SOM. If a non standard MSEL/BSEL configuration or a new clock configuration is required, a custom board managmenet controller variant can be created using this design and stored in the user flash area of the Max 10 over JTAG. While the clock generator configuration can alternatively be updated at runtime, the reference clocks of the transceivers need to be correct at the time of FPGA configuration, else a manual recalibration is required.

Required tools:
- Python 3
- [SiLabs Clock Builder Pro Version v2.13. to v2.29.](https://www.silabs.com/products/development-tools/software/clockbuilder-pro-software)
- Nios Command Shell of Quartus 17.1.

## Generating a new Clock Configuration

1. Open Clock Builder Pro.
2. Click Open Project.
3. Select hex_creator_script/Clkgen-Project.slabtimeproj.
4. Click on Output Clocks and adjust them. Each Output clock is responsible for the following function:

| Output | IO Standard | Default Frequency | User Adjustable | Default Baseboard Function
| ------ | ----------- | ----------------- | --------------- | --------------------------
| 0      | LVDS        | 135 MHz           | Yes             | Displayport refclk
| 1      | N/A         | N/A               | Yes             | Baseboard refclk 0
| 2      | LVDS        | 100 MHz           | Yes             | Baseboard refclk 1
| 3      | LVDS        | 297 MHz           | Yes             | Baseboard refclk 2
| 4      | LVDS        | 233.333 MHz       | Yes             | FPGA memory refclk
| 5      | LVDS        | 233.333 MHz       | Yes             | HPS memory refclk
| 6      | S/E         | 100 MHz           | No              | CLK_USR/config clk
| 7      | LVDS        | 100 MHz           | Yes             | LVDS bottom refclk
| 8      | LVDS        | N/A               | No              | Unused
| 9      | LVDS        | 322.265625 MHz    | Yes             | 10G Ethernet refclk

6. Click Finishe, then Export.
7. Switch to the Register File Tap.
8. Click in C Code Header File and check the 'Include pre- and post-write control register writes' checkbox.
9. Click Save to File and save as Clkgen-Registers.h

## Compile Flow

1. Adjust the required MSEL/BSEL configuration in hex_creator_script/a10_configuration.txt.
2. Create a new Clkgen-Registers.h as described in the previous section.
3. Run the compile.sh script in the NIOS command shell to create power_sequencer.pof.
4. Set the JTAG baseboard jumpers for Max 10 Configuration as described in the Baseboard Quick Start Guide.
5. Open the Quartus Programmer and scan the JTAG chain. Only a Max10 device should be in it.
6. Right click on the Max 10 and add the .pof bitstream just generated.
7. Mark both the UFM/User Flash and CFM/Config Flash Configure check boxes and Configure the Max 10.
8. Reset the jumpers to Arria 10 JTAG.
