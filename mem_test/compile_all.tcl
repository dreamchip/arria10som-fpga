# -*- tcl -*-
#-----------------------------------------------------------------------------
# This is an unpublished work, the copyright in which vests in
# DreamChip Technologies GmbH. The information contained herein is the property
# of DreamChip Technologies GmbH and is supplied without liability for errors or
# omissions. No part may be reproduced or used except as authorized by
# contract or other written permission.
# Copyright(c) DreamChip Technologies GmbH, 2016. All rights reserved.
#-----------------------------------------------------------------------------
# File      : compile_all.tcl
# Purpose   : Compile all High Bandwidth FPGA variants of Wiesmann SOM
# Creator   : EBu
#-----------------------------------------------------------------------------

package require ::quartus::flow
package require fileutil

set family "Arria 10"
set devices { "10AS016E4F29I3SG" "10AS022E4F29I3SG" "10AS027E4F29I3SG" "10AS032E4F29I3SG" "10AS048E4F29I3SG" }

post_message " "
post_message "*************************************************************************"
post_message "GHRD bitstream generator v1.0"
post_message "*************************************************************************"
post_message " "

foreach device $devices {
    set device_short [string range $device 0 8]
    post_message " "
    post_message "*************************************************************************"
    post_message "Compiling for $device"
    post_message "*************************************************************************"
    post_message " "
    puts $device
    puts $device_short
    puts "ghrd_$device_short.sof"
    
    project_open -revision mem_test mem_test
    
    set_global_assignment -name DEVICE $device

    # check for FPGA device and use proper memory controller
    if {[string range $device 5 6] > 22} {
        set_global_assignment -name VERILOG_MACRO "HIGH_BANDWIDTH"
        set_global_assignment -name QSYS_FILE mem_fpga_x32.qsys -remove
        set_global_assignment -name QSYS_FILE mem_fpga_x64.qsys
        post_message "*************************************************************************"
        post_message "Compiling x64 interface"
        post_message "*************************************************************************"
    } else {
        set_global_assignment -name QSYS_FILE mem_fpga_x32.qsys
        set_global_assignment -name QSYS_FILE mem_fpga_x64.qsys -remove
        set_global_assignment -name VERILOG_MACRO "HIGH_BANDWIDTH" -remove
        post_message "*************************************************************************"
        post_message "Compiling x32 interface"
        post_message "*************************************************************************"
    }
    
    execute_flow -compile
    
    post_message "Generating .jic/.files."
    
    file delete -force "mem_test_$device_short.sof"
    file rename "mem_test.sof" "mem_test_$device_short.sof"
    
    project_close
}
