//-----------------------------------------------------------------------------
// $HeadURL$
// $Author$
// $Rev$
// $Date$
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in
// DreamChip Technologies GmbH. The information contained herein is the property
// of DreamChip Technologies GmbH and is supplied without liability for errors or
// omissions. No part may be reproduced or used except as authorized by
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2017. All rights reserved.
//-----------------------------------------------------------------------------
// Module    : mem_test.v
// Hierarchy : Top Level
// Purpose   : Golden Hardware Reference Design for DCT Arria 10 SoM
// Creator   : Eric Bunker
// Remark    : v0.4
//-----------------------------------------------------------------------------

module mem_test (
  // Clocks & Resets ----------------------------------------------------------
  input wire         pcie_perst,
                 
  // FPGA DDR4 ----------------------------------------------------------------
  output wire [0:0]  fpga_mem_ck, 
  output wire [0:0]  fpga_mem_ck_n, 
  output wire [16:0] fpga_mem_a, 
  output wire [0:0]  fpga_mem_act_n, 
  output wire [1:0]  fpga_mem_ba, 
  output wire [0:0]  fpga_mem_bg, 
  output wire [0:0]  fpga_mem_cke, 
  output wire [0:0]  fpga_mem_cs_n, 
  output wire [0:0]  fpga_mem_odt, 
  output wire [0:0]  fpga_mem_reset_n, 
  output wire [0:0]  fpga_mem_par, 
  input wire [0:0]   fpga_mem_alert_n,
`ifdef HIGH_BANDWIDTH
  inout wire [7:0]   fpga_mem_dqs, 
  inout wire [7:0]   fpga_mem_dqs_n, 
  inout wire [63:0]  fpga_mem_dq, 
  inout wire [7:0]   fpga_mem_dbi_n,
`else
  inout wire [3:0]   fpga_mem_dqs, 
  inout wire [3:0]   fpga_mem_dqs_n, 
  inout wire [31:0]  fpga_mem_dq, 
  inout wire [3:0]   fpga_mem_dbi_n,
`endif
  input wire         fpga_mem_oct_rzqin, 
  input wire         fpga_mem_refclk, 
  
  // HPS DDR4 -----------------------------------------------------------------
  output wire [0:0]  hps_mem_ck, 
  output wire [0:0]  hps_mem_ck_n, 
  output wire [16:0] hps_mem_a, 
  output wire [0:0]  hps_mem_act_n, 
  output wire [1:0]  hps_mem_ba, 
  output wire [0:0]  hps_mem_bg, 
  output wire [0:0]  hps_mem_cke, 
  output wire [0:0]  hps_mem_cs_n, 
  output wire [0:0]  hps_mem_odt, 
  output wire [0:0]  hps_mem_reset_n, 
  output wire [0:0]  hps_mem_par, 
  input wire [0:0]   hps_mem_alert_n, 
  inout wire [4:0]   hps_mem_dqs, 
  inout wire [4:0]   hps_mem_dqs_n, 
  inout wire [39:0]  hps_mem_dq, 
  inout wire [4:0]   hps_mem_dbi_n, 
  input wire         hps_mem_oct_rzqin, 
  input wire         hps_mem_refclk
);
   
//-----------------------------------------------------------------------------
// 32/64 bit FPGA Memory
//-----------------------------------------------------------------------------
`ifdef HIGH_BANDWIDTH
  mem_fpga_x64 mem_fpga_inst (
`else
  mem_fpga_x32 mem_fpga_inst (
`endif
    .reset_reset_n    (~pcie_perst       ), 
    .refclk_clk       (fpga_mem_refclk   ), 
    .mem_mem_ck       (fpga_mem_ck       ), 
    .mem_mem_ck_n     (fpga_mem_ck_n     ), 
    .mem_mem_a        (fpga_mem_a        ), 
    .mem_mem_act_n    (fpga_mem_act_n    ), 
    .mem_mem_ba       (fpga_mem_ba       ), 
    .mem_mem_bg       (fpga_mem_bg       ), 
    .mem_mem_cke      (fpga_mem_cke      ), 
    .mem_mem_cs_n     (fpga_mem_cs_n     ), 
    .mem_mem_odt      (fpga_mem_odt      ), 
    .mem_mem_reset_n  (fpga_mem_reset_n  ), 
    .mem_mem_par      (fpga_mem_par      ), 
    .mem_mem_alert_n  (fpga_mem_alert_n  ), 
    .mem_mem_dqs      (fpga_mem_dqs      ), 
    .mem_mem_dqs_n    (fpga_mem_dqs_n    ), 
    .mem_mem_dq       (fpga_mem_dq       ), 
    .mem_mem_dbi_n    (fpga_mem_dbi_n    ), 
    .oct_oct_rzqin    (fpga_mem_oct_rzqin)  
  );

//-----------------------------------------------------------------------------
// 40 Bit HPS Memory
//-----------------------------------------------------------------------------
   
  mem_hps mem_hps_inst (
    .reset_reset_n    (~pcie_perst      ), 
    .refclk_clk       (hps_mem_refclk   ), 
    .mem_mem_ck       (hps_mem_ck       ), 
    .mem_mem_ck_n     (hps_mem_ck_n     ), 
    .mem_mem_a        (hps_mem_a        ), 
    .mem_mem_act_n    (hps_mem_act_n    ), 
    .mem_mem_ba       (hps_mem_ba       ), 
    .mem_mem_bg       (hps_mem_bg       ), 
    .mem_mem_cke      (hps_mem_cke      ), 
    .mem_mem_cs_n     (hps_mem_cs_n     ), 
    .mem_mem_odt      (hps_mem_odt      ), 
    .mem_mem_reset_n  (hps_mem_reset_n  ), 
    .mem_mem_par      (hps_mem_par      ), 
    .mem_mem_alert_n  (hps_mem_alert_n  ), 
    .mem_mem_dqs      (hps_mem_dqs      ), 
    .mem_mem_dqs_n    (hps_mem_dqs_n    ), 
    .mem_mem_dq       (hps_mem_dq       ), 
    .mem_mem_dbi_n    (hps_mem_dbi_n    ), 
    .oct_oct_rzqin    (hps_mem_oct_rzqin)  
  );

endmodule
