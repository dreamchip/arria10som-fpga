# DDR4 Memory (mem_test)

## Contents

| File               |  Description
| ------------------ | -------------------------------------------------
| compile_all.tcl    | Compiles the design for all FPGA types.
| Makefile           | Makefile to compile the design.
| mem_fpga_x32.qsys  | QSYS system of the FPGA memory for 160 and 220 kLE devices.
| mem_fpga_x64.qsys  | QSYS system of the FPGA memory for 270, 320 and 480 kLE devices.
| mem_hps.qsys       | QSYS system of the HPS memory, full 40 bit without ECC.
| mem_test.qpf       | Quartus project file.
| mem_test.qsf       | Quartus setting file.
| mem_test.sdc       | Default clock constraints.
| mem_test.stp       | Signaltap of the test pattern generator/checker.
| mem_test.v         | Verilog top level.

[External Memory Interfaces Intel® Arria® 10 FPGA IP Design Example User Guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug-20118.pdf)

The mem_test design includes both DDR4 controllers of the SOM and the test pattern generator and checker modules of the Quartus example design for them. The HPS is not active in this design, so the HPS DDR4 interface is used from the FPGA in its whole 40 bit width. Depending on the size of the FPGA used, the FPGA DDR4 interface is either 32 or 64 bit wide.
