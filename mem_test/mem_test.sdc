#-----------------------------------------------------------------------------
# $HeadURL$
# $Author$
# $Rev$
# $Date$
#-----------------------------------------------------------------------------
# This is an unpublished work, the copyright in which vests in
# DreamChip Technologies GmbH. The information contained herein is the property
# of DreamChip Technologies GmbH and is supplied without liability for errors or
# omissions. No part may be reproduced or used except as authorized by
# contract or other written permission.
# Copyright(c) DreamChip Technologies GmbH, 2017. All rights reserved.
#-----------------------------------------------------------------------------
# Module    : mem_test.sdc
# Hierarchy : Top Level
# Purpose   : Golden Hardware Reference Design for DCT Arria 10 SoM
# Creator   : Eric Bunker
# Remark    : v0.4
#-----------------------------------------------------------------------------

create_clock -period "233.333 MHz"    hps_mem_refclk
create_clock -period "233.333 MHz"    fpga_mem_refclk
