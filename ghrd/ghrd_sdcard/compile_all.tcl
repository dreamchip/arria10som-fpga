# -*- tcl -*-
#-----------------------------------------------------------------------------
# This is an unpublished work, the copyright in which vests in
# DreamChip Technologies GmbH. The information contained herein is the property
# of DreamChip Technologies GmbH and is supplied without liability for errors or
# omissions. No part may be reproduced or used except as authorized by
# contract or other written permission.
# Copyright(c) DreamChip Technologies GmbH, 2016. All rights reserved.
#-----------------------------------------------------------------------------
# File      : compile_all.tcl
# Purpose   : Compile all High Bandwidth FPGA variants of Wiesmann SOM
# Creator   : EBu
#-----------------------------------------------------------------------------

package require ::quartus::flow
package require fileutil

set family "Arria 10"
set devices { "10AS016E4F29I3SG" "10AS022E4F29I3SG" "10AS027E4F29I3SG" "10AS032E4F29I3SG" "10AS048E4F29I3SG" }

post_message " "
post_message "*************************************************************************"
post_message "GHRD bitstream generator v1.0"
post_message "*************************************************************************"
post_message " "

foreach device $devices {
  set device_short [string range $device 0 8]
  post_message " "
  post_message "*************************************************************************"
  post_message "Compiling for $device"
  post_message "*************************************************************************"
  post_message " "
  puts $device
  puts $device_short
  puts "ghrd_$device_short.sof"
  
  project_open -revision ghrd ghrd
  
  set_global_assignment -name DEVICE $device
  
  execute_flow -compile
  
  post_message "Generating .jic/.files."
  
  file delete -force "ghrd_$device_short.sof"
  file rename "ghrd.sof" "ghrd_$device_short.sof"
  execute_module -tool cpf -args "-c --hps -s $device_short -o bitstream_compression=on ghrd_$device_short.sof ghrd_$device_short.rbf"
  
  project_close
}
