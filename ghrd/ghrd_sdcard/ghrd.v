//-----------------------------------------------------------------------------
// $HeadURL$
// $Author$
// $Rev$
// $Date$
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in
// DreamChip Technologies GmbH. The information contained herein is the property
// of DreamChip Technologies GmbH and is supplied without liability for errors or
// omissions. No part may be reproduced or used except as authorized by
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2017. All rights reserved.
//-----------------------------------------------------------------------------
// Module    : ghrd.v
// Hierarchy : Top Level
// Purpose   : Golden Hardware Reference Design for DCT Arria 10 SoM
// Creator   : Eric Bunker
// Remark    : v0.4
//-----------------------------------------------------------------------------

module ghrd (
  // LVDS & GPIO, available on all devices ------------------------------------
  input  wire          lvds_b_clk_in_ext,
  input  wire          lvds_b_clk_in_int,
  output wire  [1:0]   lvds_b_clk_out,
  input  wire  [8:0]   lvds_b_data_in,
  output wire  [7:0]   lvds_b_data_out,
  //input  wire  [1:0]   lvds_t_clk_in_ext,
  //output wire  [1:0]   lvds_t_clk_out,
  //input  wire  [3:0]   lvds_t_data_in,
  //output wire  [3:0]   lvds_t_data_out,

  // FPGA DDR4 ----------------------------------------------------------------
  //output wire  [0:0]   fpga_mem_ck,      
  //output wire  [0:0]   fpga_mem_ck_n,    
  //output wire [16:0]   fpga_mem_a,       
  //output wire  [0:0]   fpga_mem_act_n,   
  //output wire  [1:0]   fpga_mem_ba,      
  //output wire  [0:0]   fpga_mem_bg,      
  //output wire  [0:0]   fpga_mem_cke,     
  //output wire  [0:0]   fpga_mem_cs_n,    
  //output wire  [0:0]   fpga_mem_odt,     
  //output wire  [0:0]   fpga_mem_reset_n, 
  //output wire  [0:0]   fpga_mem_par,     
  //input  wire  [0:0]   fpga_mem_alert_n, 
  //inout  wire  [7:0]   fpga_mem_dqs,     
  //inout  wire  [7:0]   fpga_mem_dqs_n,   
  //inout  wire [63:0]   fpga_mem_dq,      
  //inout  wire  [7:0]   fpga_mem_dbi_n,  
  //input  wire          fpga_oct_rzqin,   
  //input  wire          fpga_mem_refclk, 
  
  // PCIe Gen3 x4 ------------------------------------------------------
  input  wire          pcie_perst_n,
  input  wire          pcie_refclk,
  //input  wire [3:0]    pcie_rx,
  //output wire [3:0]    pcie_tx,
  
  // Displayport RX & TX ------------------------------------------------------
  //inout  wire          dp_reset_n,
  //input  wire          dp_refclk,
  //output wire          dp_rx_hpd_out,
  //input  wire          dp_rx_aux_in,
  //output wire          dp_rx_aux_out,
  //output wire          dp_rx_aux_oe,
  //input  wire [3:0]    dp_rx_data,
  //input wire           dp_tx_hpd_in,
  //inout wire           dp_tx_aux_p,
  //inout wire           dp_tx_aux_n,
  //output wire [3:0]    dp_tx_data,


  // SDI RX & TX ------------------------------------------------------
  input  wire          sdi_refclk,
  //inout  wire          genl_reset_n,
  input  wire          genl_tof,
  //output wire          genl_href,
  //output wire          genl_vref,
  //input  wire          sdi_rx_data,
  //output wire          sdi_tx_data,
  
  // 10G Ethernet SFP+ --------------------------------------------------------
  input  wire          xge_refclk,
  //input  wire          xge_rx_data,
  //output wire          xge_tx_data,
  
  // Misc.
  //inout  wire          fan_ctrl, 
  
  // HPS sd card -----------------------------------------------------------------
  output wire          hps_sd_clk,  
  inout  wire          hps_sd_cmd,  
  inout  wire  [7:0]   hps_sd_data,    

  // HPS EMAC1 ----------------------------------------------------------------
  output wire          hps_emac1_tx_clk,    
  output wire  [3:0]   hps_emac1_txd,       
  input  wire          hps_emac1_rx_ctl,    
  output wire          hps_emac1_tx_ctl,    
  input  wire          hps_emac1_rx_clk,    
  input  wire  [3:0]   hps_emac1_rxd,       
  inout  wire          hps_emac1_mdio,      
  output wire          hps_emac1_mdc,             
  
  // HPS USB0 -----------------------------------------------------------------
  inout  wire  [7:0]   hps_usb0_data, 
  input  wire          hps_usb0_clk,    
  output wire          hps_usb0_stp,    
  input  wire          hps_usb0_dir,    
  input  wire          hps_usb0_nxt,    
  
  // HPS USB1 -----------------------------------------------------------------
  inout  wire  [7:0]   hps_usb1_data, 
  input  wire          hps_usb1_clk,    
  output wire          hps_usb1_stp,    
  input  wire          hps_usb1_dir,    
  input  wire          hps_usb1_nxt,   
  
  // HPS I2C ------------------------------------------------------------------
  inout  wire          hps_i2c_sda, 
  inout  wire          hps_i2c_scl, 
  
  // Clock Generator//MAC EEPROM I2C ------------------------------------------
  inout  wire          clkgen_i2c_cl,
  inout  wire          clkgen_i2c_da,
  
  // HPS UART -----------------------------------------------------------------
  input  wire          hps_uart_rx,         
  output wire          hps_uart_tx,       
  
  // HPS DDR4 -----------------------------------------------------------------
  output wire  [0:0]   hps_mem_ck,         
  output wire  [0:0]   hps_mem_ck_n,       
  output wire [16:0]   hps_mem_a,          
  output wire  [0:0]   hps_mem_act_n,      
  output wire  [1:0]   hps_mem_ba,         
  output wire  [0:0]   hps_mem_bg,         
  output wire  [0:0]   hps_mem_cke,        
  output wire  [0:0]   hps_mem_cs_n,       
  output wire  [0:0]   hps_mem_odt,        
  output wire  [0:0]   hps_mem_reset_n,    
  output wire  [0:0]   hps_mem_par,        
  input  wire  [0:0]   hps_mem_alert_n,    
  inout  wire  [3:0]   hps_mem_dqs,        
  inout  wire  [3:0]   hps_mem_dqs_n,      
  inout  wire [31:0]   hps_mem_dq,         
  inout  wire  [3:0]   hps_mem_dbi_n,      
  input  wire          hps_mem_oct_rzqin,  
  input  wire          hps_mem_refclk
);

  wire       reset_n           ;
  wire       hps_cold_reset_n  ;
  wire       hps_h2f_user1_clk ;
  
  wire       const_zero_sig /*synthesis keep*/; 
  wire       i2c0_scl_in_clk;
  wire       i2c0_clk_clk;   
  wire       i2c0_sda_i;     
  wire       i2c0_sda_oe;    
  wire       i2c1_scl_in_clk;
  wire       i2c1_clk_clk;   
  wire       i2c1_sda_i;     
  wire       i2c1_sda_oe;    

//-----------------------------------------------------------------------------
// Hard Processing System
//-----------------------------------------------------------------------------

  hps hps_inst (
    .hps_io_hps_io_phery_sdmmc_CMD         (hps_sd_cmd            ), //                           hps_io.hps_io_phery_sdmmc_CMD
    .hps_io_hps_io_phery_sdmmc_D0          (hps_sd_data[0]        ), //                                 .hps_io_phery_sdmmc_D0
    .hps_io_hps_io_phery_sdmmc_D1          (hps_sd_data[1]        ), //                                 .hps_io_phery_sdmmc_D1
    .hps_io_hps_io_phery_sdmmc_D2          (hps_sd_data[2]        ), //                                 .hps_io_phery_sdmmc_D2
    .hps_io_hps_io_phery_sdmmc_D3          (hps_sd_data[3]        ), //                                 .hps_io_phery_sdmmc_D3
    .hps_io_hps_io_phery_sdmmc_CCLK        (hps_sd_clk           ), //                                 .hps_io_phery_sdmmc_CCLK
    .hps_io_hps_io_phery_emac1_TX_CLK      (hps_emac1_tx_clk        ), //                                 .hps_io_phery_emac1_tx_clk
    .hps_io_hps_io_phery_emac1_TXD0        (hps_emac1_txd[0]        ), //                                 .hps_io_phery_emac1_txd0
    .hps_io_hps_io_phery_emac1_TXD1        (hps_emac1_txd[1]        ), //                                 .hps_io_phery_emac1_txd1
    .hps_io_hps_io_phery_emac1_TXD2        (hps_emac1_txd[2]        ), //                                 .hps_io_phery_emac1_txd2
    .hps_io_hps_io_phery_emac1_TXD3        (hps_emac1_txd[3]        ), //                                 .hps_io_phery_emac1_txd3
    .hps_io_hps_io_phery_emac1_RX_CTL      (hps_emac1_rx_ctl        ), //                                 .hps_io_phery_emac1_rx_ctl
    .hps_io_hps_io_phery_emac1_TX_CTL      (hps_emac1_tx_ctl        ), //                                 .hps_io_phery_emac1_tx_ctl
    .hps_io_hps_io_phery_emac1_RX_CLK      (hps_emac1_rx_clk        ), //                                 .hps_io_phery_emac1_rx_clk
    .hps_io_hps_io_phery_emac1_RXD0        (hps_emac1_rxd[0]        ), //                                 .hps_io_phery_emac1_rxd0
    .hps_io_hps_io_phery_emac1_RXD1        (hps_emac1_rxd[1]        ), //                                 .hps_io_phery_emac1_rxd1
    .hps_io_hps_io_phery_emac1_RXD2        (hps_emac1_rxd[2]        ), //                                 .hps_io_phery_emac1_rxd2
    .hps_io_hps_io_phery_emac1_RXD3        (hps_emac1_rxd[3]        ), //                                 .hps_io_phery_emac1_rxd3
    .hps_io_hps_io_phery_emac1_MDIO        (hps_emac1_mdio          ), //                                 .hps_io_phery_emac1_mdio
    .hps_io_hps_io_phery_emac1_MDC         (hps_emac1_mdc           ), //                                 .hps_io_phery_emac1_mdc
    .hps_io_hps_io_phery_usb0_DATA0        (hps_usb0_data[0]        ), //                                 .hps_io_phery_usb0_data0
    .hps_io_hps_io_phery_usb0_DATA1        (hps_usb0_data[1]        ), //                                 .hps_io_phery_usb0_data1
    .hps_io_hps_io_phery_usb0_DATA2        (hps_usb0_data[2]        ), //                                 .hps_io_phery_usb0_data2
    .hps_io_hps_io_phery_usb0_DATA3        (hps_usb0_data[3]        ), //                                 .hps_io_phery_usb0_data3
    .hps_io_hps_io_phery_usb0_DATA4        (hps_usb0_data[4]        ), //                                 .hps_io_phery_usb0_data4
    .hps_io_hps_io_phery_usb0_DATA5        (hps_usb0_data[5]        ), //                                 .hps_io_phery_usb0_data5
    .hps_io_hps_io_phery_usb0_DATA6        (hps_usb0_data[6]        ), //                                 .hps_io_phery_usb0_data6
    .hps_io_hps_io_phery_usb0_DATA7        (hps_usb0_data[7]        ), //                                 .hps_io_phery_usb0_data7
    .hps_io_hps_io_phery_usb0_CLK          (hps_usb0_clk            ), //                                 .hps_io_phery_usb0_clk
    .hps_io_hps_io_phery_usb0_STP          (hps_usb0_stp            ), //                                 .hps_io_phery_usb0_stp
    .hps_io_hps_io_phery_usb0_DIR          (hps_usb0_dir            ), //                                 .hps_io_phery_usb0_dir
    .hps_io_hps_io_phery_usb0_NXT          (hps_usb0_nxt            ), //                                 .hps_io_phery_usb0_nxt
    .hps_io_hps_io_phery_usb1_DATA0        (hps_usb1_data[0]        ), //                                 .hps_io_phery_usb1_data0
    .hps_io_hps_io_phery_usb1_DATA1        (hps_usb1_data[1]        ), //                                 .hps_io_phery_usb1_data1
    .hps_io_hps_io_phery_usb1_DATA2        (hps_usb1_data[2]        ), //                                 .hps_io_phery_usb1_data2
    .hps_io_hps_io_phery_usb1_DATA3        (hps_usb1_data[3]        ), //                                 .hps_io_phery_usb1_data3
    .hps_io_hps_io_phery_usb1_DATA4        (hps_usb1_data[4]        ), //                                 .hps_io_phery_usb1_data4
    .hps_io_hps_io_phery_usb1_DATA5        (hps_usb1_data[5]        ), //                                 .hps_io_phery_usb1_data5
    .hps_io_hps_io_phery_usb1_DATA6        (hps_usb1_data[6]        ), //                                 .hps_io_phery_usb1_data6
    .hps_io_hps_io_phery_usb1_DATA7        (hps_usb1_data[7]        ), //                                 .hps_io_phery_usb1_data7
    .hps_io_hps_io_phery_usb1_CLK          (hps_usb1_clk            ), //                                 .hps_io_phery_usb1_clk
    .hps_io_hps_io_phery_usb1_STP          (hps_usb1_stp            ), //                                 .hps_io_phery_usb1_stp
    .hps_io_hps_io_phery_usb1_DIR          (hps_usb1_dir            ), //                                 .hps_io_phery_usb1_dir
    .hps_io_hps_io_phery_usb1_NXT          (hps_usb1_nxt            ), //                                 .hps_io_phery_usb1_nxt
    .hps_io_hps_io_phery_uart1_RX          (hps_uart_rx             ), //                                 .rx
    .hps_io_hps_io_phery_uart1_TX          (hps_uart_tx             ), //                                 .tx
    .hps_mem_refclk_clk                    (hps_mem_refclk          ), //                   hps_mem_refclk.clk
    .hps_mem_oct_oct_rzqin                 (hps_mem_oct_rzqin       ), //                      hps_mem_oct.oct_rzqin
    .hps_mem_mem_ck                        (hps_mem_ck              ), //                          hps_mem.mem_ck
    .hps_mem_mem_ck_n                      (hps_mem_ck_n            ), //                                 .mem_ck_n
    .hps_mem_mem_a                         (hps_mem_a               ), //                                 .mem_a
    .hps_mem_mem_act_n                     (hps_mem_act_n           ), //                                 .mem_act_n
    .hps_mem_mem_ba                        (hps_mem_ba              ), //                                 .mem_ba
    .hps_mem_mem_bg                        (hps_mem_bg              ), //                                 .mem_bg
    .hps_mem_mem_cke                       (hps_mem_cke             ), //                                 .mem_cke
    .hps_mem_mem_cs_n                      (hps_mem_cs_n            ), //                                 .mem_cs_n
    .hps_mem_mem_odt                       (hps_mem_odt             ), //                                 .mem_odt
    .hps_mem_mem_reset_n                   (hps_mem_reset_n         ), //                                 .mem_reset_n
    .hps_mem_mem_par                       (hps_mem_par             ), //                                 .mem_par
    .hps_mem_mem_alert_n                   (hps_mem_alert_n         ), //                                 .mem_alert_n
    .hps_mem_mem_dqs                       (hps_mem_dqs             ), //                                 .mem_dqs
    .hps_mem_mem_dqs_n                     (hps_mem_dqs_n           ), //                                 .mem_dqs_n
    .hps_mem_mem_dq                        (hps_mem_dq              ), //                                 .mem_dq
    .hps_mem_mem_dbi_n                     (hps_mem_dbi_n           ), //                                 .mem_dbi_n
    .hps_mem_reset_reset_n                 (reset_n                 ), //                    hps_mem_reset.reset_n
    .arria10_hps_0_spim0_mosi_o            (hps_spim_mosi           ), //              arria10_hps_0_spim0.mosi_o
    .arria10_hps_0_spim0_miso_i            (hps_spim_miso           ), //                                 .miso_i
    .arria10_hps_0_spim0_ss0_n_o           (hps_spim_ss_n           ), //                                 .ss0_n_o
    .arria10_hps_0_spim0_ss_in_n           (1'b1                    ), //                                 .ss_in_n
    .arria10_hps_0_spim0_sclk_out_clk      (hps_spim_clk            ), //     arria10_hps_0_spim0_sclk_out.clk
    .arria10_hps_0_i2c0_scl_in_clk         (i2c0_scl_in_clk         ), //        arria10_hps_0_i2c0_scl_in.clk
    .arria10_hps_0_i2c0_clk_clk            (i2c0_clk_clk            ), //           arria10_hps_0_i2c0_clk.clk
    .arria10_hps_0_i2c0_sda_i              (i2c0_sda_i              ), //               arria10_hps_0_i2c0.sda_i
    .arria10_hps_0_i2c0_sda_oe             (i2c0_sda_oe             ), //                                 .sda_oe
    .hps_cold_reset_reset_n                (hps_cold_reset_n        ), //                   hps_cold_reset.reset_n
    .hps_h2f_user1_clock_clk               (hps_h2f_user1_clk       ), //              hps_h2f_user1_clock.clk
    .arria10_hps_0_i2c1_scl_in_clk         (i2c1_scl_in_clk         ), //        arria10_hps_0_i2c1_scl_in.clk
    .arria10_hps_0_i2c1_clk_clk            (i2c1_clk_clk            ), //           arria10_hps_0_i2c1_clk.clk
    .arria10_hps_0_i2c1_sda_i              (i2c1_sda_i              ), //               arria10_hps_0_i2c1.sda_i
    .arria10_hps_0_i2c1_sda_oe             (i2c1_sda_oe             )  //                                 .sda_oe
    );

      
//-----------------------------------------------------------------------------
// I2C Output
//-----------------------------------------------------------------------------  

  // Open drain fitter fix 
  assign const_zero_sig = 1'b0;
  
  assign i2c0_scl_in_clk = hps_i2c_scl;
  assign hps_i2c_scl = (i2c0_clk_clk) ? const_zero_sig : 1'bz;
  assign i2c0_sda_i = hps_i2c_sda;
  assign hps_i2c_sda = (i2c0_sda_oe) ? const_zero_sig : 1'bz;
  
  assign i2c1_scl_in_clk = clkgen_i2c_cl;
  assign clkgen_i2c_cl = (i2c1_clk_clk) ? const_zero_sig : 1'bz;
  assign i2c1_sda_i = clkgen_i2c_da;
  assign clkgen_i2c_da = (i2c1_sda_oe) ? const_zero_sig : 1'bz;

endmodule
