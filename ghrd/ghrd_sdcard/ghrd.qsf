# -------------------------------------------------------------------------- #
#
# Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions
# and other software and tools, and its AMPP partner logic
# functions, and any output files from any of the foregoing
# (including device programming or simulation files), and any
# associated documentation or information are expressly subject
# to the terms and conditions of the Altera Program License
# Subscription Agreement, the Altera Quartus Prime License Agreement,
# the Altera MegaCore Function License Agreement, or other
# applicable license agreement, including, without limitation,
# that your use is for the sole purpose of programming logic
# devices manufactured by Altera and sold by Altera or its
# authorized distributors.  Please refer to the applicable
# agreement for further details.
#
# -------------------------------------------------------------------------- #
#
# Quartus Prime
# Version 15.1.0 Build 185 10/21/2015 SJ Standard Edition
# Date created = 11:23:35  May 31, 2016
#
# -------------------------------------------------------------------------- #
#
# Notes:
#
# 1) The default values for assignments are stored in the file:
#   a10_som_assignment_defaults.qdf
#    If this file doesn't exist, see file:
#   assignment_defaults.qdf
#
# 2) Altera recommends that you do not modify this file. This
#    file is updated automatically by the Quartus Prime software
#    and any changes you make may be lost or overwritten.
#
# -------------------------------------------------------------------------- #


set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name DEVICE 10AS048E4F29I3SG
set_global_assignment -name TOP_LEVEL_ENTITY ghrd
set_global_assignment -name ORIGINAL_QUARTUS_VERSION 17.1.0
set_global_assignment -name PROJECT_CREATION_TIME_DATE "15:07:36  JUNE 22, 2016"
set_global_assignment -name LAST_QUARTUS_VERSION "17.1.0 Standard Edition"
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 2
set_global_assignment -name ENABLE_OCT_DONE OFF
set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X4"
set_global_assignment -name USE_CONFIGURATION_DEVICE ON
set_global_assignment -name STRATIXII_CONFIGURATION_DEVICE EPCQL256
set_global_assignment -name CRC_ERROR_OPEN_DRAIN ON
set_global_assignment -name FORCE_CONFIGURATION_VCCIO ON
set_global_assignment -name CONFIGURATION_VCCIO_LEVEL 1.8V
set_global_assignment -name RESERVE_DATA0_AFTER_CONFIGURATION "USE AS REGULAR IO"
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall
set_global_assignment -name ACTIVE_SERIAL_CLOCK CLKUSR
set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "1.8 V"
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
set_global_assignment -name PROJECT_IP_REGENERATION_POLICY ALWAYS_REGENERATE_IP
set_global_assignment -name HPS_EARLY_IO_RELEASE ON

#******************************************************************************
# Source Files
#******************************************************************************

set_global_assignment -name QSYS_FILE hps.qsys
set_global_assignment -name SDC_FILE ghrd.sdc
set_global_assignment -name VERILOG_FILE ghrd.v
#******************************************************************************
# Location
#******************************************************************************

# HPS SD Card
set_location_assignment PIN_D12 -to hps_sd_clk
set_location_assignment PIN_F14 -to hps_sd_data[3]
set_location_assignment PIN_H12 -to hps_sd_data[2]
set_location_assignment PIN_J12 -to hps_sd_data[1]
set_location_assignment PIN_F12 -to hps_sd_data[0]
set_location_assignment PIN_G16 -to hps_sd_cmd

# HPS USB0
set_location_assignment PIN_C18 -to hps_usb0_clk
set_location_assignment PIN_D18 -to hps_usb0_data[7]
set_location_assignment PIN_D19 -to hps_usb0_data[6]
set_location_assignment PIN_E17 -to hps_usb0_data[5]
set_location_assignment PIN_E16 -to hps_usb0_data[4]
set_location_assignment PIN_F19 -to hps_usb0_data[3]
set_location_assignment PIN_E19 -to hps_usb0_data[2]
set_location_assignment PIN_D20 -to hps_usb0_data[1]
set_location_assignment PIN_C16 -to hps_usb0_data[0]
set_location_assignment PIN_C17 -to hps_usb0_dir
set_location_assignment PIN_E20 -to hps_usb0_nxt
set_location_assignment PIN_D17 -to hps_usb0_stp

# HPS USB 1
set_location_assignment PIN_K22 -to hps_usb1_stp
set_location_assignment PIN_K23 -to hps_usb1_clk
set_location_assignment PIN_H23 -to hps_usb1_data[7]
set_location_assignment PIN_J23 -to hps_usb1_data[6]
set_location_assignment PIN_K21 -to hps_usb1_data[5]
set_location_assignment PIN_J20 -to hps_usb1_data[4]
set_location_assignment PIN_H22 -to hps_usb1_data[3]
set_location_assignment PIN_J22 -to hps_usb1_data[2]
set_location_assignment PIN_H20 -to hps_usb1_data[1]
set_location_assignment PIN_K20 -to hps_usb1_data[0]
set_location_assignment PIN_K19 -to hps_usb1_dir
set_location_assignment PIN_H21 -to hps_usb1_nxt
set_location_assignment PIN_K15 -to hps_uart_tx
set_location_assignment PIN_F13 -to hps_uart_rx

# HPS GPIO
set_location_assignment PIN_H16 -to fan_ctrl
set_location_assignment PIN_H17 -to genl_reset_n
set_location_assignment PIN_G15 -to dp_reset_n
set_location_assignment PIN_E12 -to hps_usb1_reset_n

# HPS EMAC1
set_location_assignment PIN_F16 -to hps_emac1_mdc
set_location_assignment PIN_H15 -to hps_emac1_mdio
set_location_assignment PIN_F21 -to hps_emac1_rx_clk
set_location_assignment PIN_G21 -to hps_emac1_rx_ctl
set_location_assignment PIN_E21 -to hps_emac1_rxd[3]
set_location_assignment PIN_D22 -to hps_emac1_rxd[2]
set_location_assignment PIN_F22 -to hps_emac1_rxd[1]
set_location_assignment PIN_E22 -to hps_emac1_rxd[0]
set_location_assignment PIN_F23 -to hps_emac1_tx_clk
set_location_assignment PIN_G23 -to hps_emac1_tx_ctl
set_location_assignment PIN_E23 -to hps_emac1_txd[3]
set_location_assignment PIN_D23 -to hps_emac1_txd[2]
set_location_assignment PIN_C22 -to hps_emac1_txd[1]
set_location_assignment PIN_C23 -to hps_emac1_txd[0]

# LVDS Bottom
set_location_assignment PIN_AB16 -to lvds_b_clk_in_int
set_location_assignment PIN_AG14 -to lvds_b_clk_in_ext
set_location_assignment PIN_AC16 -to lvds_b_clk_out[0]
set_location_assignment PIN_AE19 -to lvds_b_clk_out[1]

set_location_assignment PIN_AD20 -to lvds_b_data_in[0]
set_location_assignment PIN_AD18 -to lvds_b_data_in[1]
set_location_assignment PIN_AF17 -to lvds_b_data_in[2]
set_location_assignment PIN_AE12 -to lvds_b_data_in[3]
set_location_assignment PIN_AE15 -to lvds_b_data_in[4]
set_location_assignment PIN_AC12 -to lvds_b_data_in[5]
set_location_assignment PIN_AD13 -to lvds_b_data_in[6]
set_location_assignment PIN_AC13 -to lvds_b_data_in[7]
set_location_assignment PIN_AB14 -to lvds_b_data_in[8]
set_location_assignment PIN_AG16 -to lvds_b_data_out[0]
set_location_assignment PIN_AG18 -to lvds_b_data_out[1]
set_location_assignment PIN_AE17 -to lvds_b_data_out[2]
set_location_assignment PIN_AF12 -to lvds_b_data_out[3]
set_location_assignment PIN_AE16 -to lvds_b_data_out[4]
set_location_assignment PIN_AE11 -to lvds_b_data_out[5]
set_location_assignment PIN_AC15 -to lvds_b_data_out[6]
set_location_assignment PIN_AA12 -to lvds_b_data_out[7]

# FPGA Memory x32
set_location_assignment PIN_L2 -to fpga_mem_refclk
set_location_assignment PIN_AF6 -to fpga_mem_dqs[0]
set_location_assignment PIN_AE6 -to fpga_mem_dqs_n[0]
set_location_assignment PIN_AA2 -to fpga_mem_dqs[3]
set_location_assignment PIN_AB3 -to fpga_mem_dqs_n[3]
set_location_assignment PIN_W5 -to fpga_mem_dqs[2]
set_location_assignment PIN_Y5 -to fpga_mem_dqs_n[2]
set_location_assignment PIN_AE2 -to fpga_mem_dqs[1]
set_location_assignment PIN_AD2 -to fpga_mem_dqs_n[1]
set_location_assignment PIN_T1 -to fpga_mem_a[0]
set_location_assignment PIN_U4 -to fpga_mem_par[0]
set_location_assignment PIN_N3 -to fpga_mem_a[16]
set_location_assignment PIN_N2 -to fpga_mem_a[15]
set_location_assignment PIN_J3 -to fpga_mem_a[14]
set_location_assignment PIN_H2 -to fpga_mem_a[13]
set_location_assignment PIN_J2 -to fpga_mem_a[12]
set_location_assignment PIN_M1 -to fpga_mem_a[11]
set_location_assignment PIN_N1 -to fpga_mem_a[10]
set_location_assignment PIN_G1 -to fpga_mem_a[9]
set_location_assignment PIN_H1 -to fpga_mem_a[8]
set_location_assignment PIN_P2 -to fpga_mem_a[7]
set_location_assignment PIN_R2 -to fpga_mem_a[6]
set_location_assignment PIN_T3 -to fpga_mem_a[5]
set_location_assignment PIN_T2 -to fpga_mem_a[4]
set_location_assignment PIN_K1 -to fpga_mem_a[3]
set_location_assignment PIN_L1 -to fpga_mem_a[2]
set_location_assignment PIN_R1 -to fpga_mem_a[1]
set_location_assignment PIN_V2 -to fpga_mem_act_n[0]
set_location_assignment PIN_AE4 -to fpga_mem_alert_n[0]
set_location_assignment PIN_M3 -to fpga_mem_ba[1]
set_location_assignment PIN_L4 -to fpga_mem_ba[0]
set_location_assignment PIN_M4 -to fpga_mem_bg[0]
set_location_assignment PIN_V1 -to fpga_mem_ck[0]
set_location_assignment PIN_U1 -to fpga_mem_ck_n[0]
set_location_assignment PIN_U6 -to fpga_mem_cke[0]
set_location_assignment PIN_W2 -to fpga_mem_cs_n[0]
set_location_assignment PIN_K2 -to fpga_oct_rzqin
set_location_assignment PIN_V3 -to fpga_mem_reset_n[0]
set_location_assignment PIN_AA7 -to fpga_mem_dbi_n[3]
set_location_assignment PIN_AA9 -to fpga_mem_dbi_n[2]
set_location_assignment PIN_AG3 -to fpga_mem_dbi_n[1]
set_location_assignment PIN_AC6 -to fpga_mem_dbi_n[0]
set_location_assignment PIN_AA3 -to fpga_mem_dq[31]
set_location_assignment PIN_AB4 -to fpga_mem_dq[30]
set_location_assignment PIN_AC5 -to fpga_mem_dq[29]
set_location_assignment PIN_AB5 -to fpga_mem_dq[28]
set_location_assignment PIN_AA1 -to fpga_mem_dq[27]
set_location_assignment PIN_AA4 -to fpga_mem_dq[26]
set_location_assignment PIN_AB1 -to fpga_mem_dq[25]
set_location_assignment PIN_AB6 -to fpga_mem_dq[24]
set_location_assignment PIN_W4 -to fpga_mem_dq[23]
set_location_assignment PIN_Y7 -to fpga_mem_dq[22]
set_location_assignment PIN_Y2 -to fpga_mem_dq[21]
set_location_assignment PIN_W7 -to fpga_mem_dq[20]
set_location_assignment PIN_Y4 -to fpga_mem_dq[19]
set_location_assignment PIN_AA8 -to fpga_mem_dq[18]
set_location_assignment PIN_Y1 -to fpga_mem_dq[17]
set_location_assignment PIN_Y6 -to fpga_mem_dq[16]
set_location_assignment PIN_AF2 -to fpga_mem_dq[15]
set_location_assignment PIN_AE1 -to fpga_mem_dq[14]
set_location_assignment PIN_AF3 -to fpga_mem_dq[13]
set_location_assignment PIN_AC2 -to fpga_mem_dq[12]
set_location_assignment PIN_AF1 -to fpga_mem_dq[11]
set_location_assignment PIN_AC3 -to fpga_mem_dq[10]
set_location_assignment PIN_AG1 -to fpga_mem_dq[9]
set_location_assignment PIN_AC1 -to fpga_mem_dq[8]
set_location_assignment PIN_AC7 -to fpga_mem_dq[7]
set_location_assignment PIN_AD4 -to fpga_mem_dq[6]
set_location_assignment PIN_AG4 -to fpga_mem_dq[5]
set_location_assignment PIN_AE5 -to fpga_mem_dq[4]
set_location_assignment PIN_AH3 -to fpga_mem_dq[3]
set_location_assignment PIN_AF4 -to fpga_mem_dq[2]
set_location_assignment PIN_AH2 -to fpga_mem_dq[1]
set_location_assignment PIN_AD5 -to fpga_mem_dq[0]
set_location_assignment PIN_V5 -to fpga_mem_odt[0]

# LVDS Top
set_location_assignment PIN_G8 -to lvds_t_clk_in_ext[0]
set_location_assignment PIN_C7 -to lvds_t_clk_in_ext[1]
set_location_assignment PIN_H8 -to lvds_t_clk_out[1]
set_location_assignment PIN_E6 -to lvds_t_clk_out[0]

set_location_assignment PIN_A6 -to lvds_t_data_in[3]
set_location_assignment PIN_C5 -to lvds_t_data_in[2]
set_location_assignment PIN_D5 -to lvds_t_data_in[1]
set_location_assignment PIN_F9 -to lvds_t_data_in[0]
set_location_assignment PIN_B5 -to lvds_t_data_out[3]
set_location_assignment PIN_K9 -to lvds_t_data_out[2]
set_location_assignment PIN_L9 -to lvds_t_data_out[1]
set_location_assignment PIN_F6 -to lvds_t_data_out[0]

# FPGA Memory x64
set_location_assignment PIN_F4 -to fpga_mem_dqs[4]
set_location_assignment PIN_E4 -to fpga_mem_dqs_n[4]
set_location_assignment PIN_K5 -to fpga_mem_dqs[7]
set_location_assignment PIN_J5 -to fpga_mem_dqs_n[7]
set_location_assignment PIN_R4 -to fpga_mem_dqs[6]
set_location_assignment PIN_R5 -to fpga_mem_dqs_n[6]
set_location_assignment PIN_H3 -to fpga_mem_dqs[5]
set_location_assignment PIN_J4 -to fpga_mem_dqs_n[5]
set_location_assignment PIN_M5 -to fpga_mem_dbi_n[7]
set_location_assignment PIN_V8 -to fpga_mem_dbi_n[6]
set_location_assignment PIN_C2 -to fpga_mem_dbi_n[5]
set_location_assignment PIN_G6 -to fpga_mem_dbi_n[4]
set_location_assignment PIN_R6 -to fpga_mem_dq[63]
set_location_assignment PIN_P8 -to fpga_mem_dq[62]
set_location_assignment PIN_R7 -to fpga_mem_dq[61]
set_location_assignment PIN_N5 -to fpga_mem_dq[60]
set_location_assignment PIN_P9 -to fpga_mem_dq[59]
set_location_assignment PIN_M6 -to fpga_mem_dq[58]
set_location_assignment PIN_P7 -to fpga_mem_dq[57]
set_location_assignment PIN_N6 -to fpga_mem_dq[56]
set_location_assignment PIN_U5 -to fpga_mem_dq[55]
set_location_assignment PIN_T6 -to fpga_mem_dq[54]
set_location_assignment PIN_T4 -to fpga_mem_dq[53]
set_location_assignment PIN_T9 -to fpga_mem_dq[52]
set_location_assignment PIN_T7 -to fpga_mem_dq[51]
set_location_assignment PIN_P4 -to fpga_mem_dq[50]
set_location_assignment PIN_P3 -to fpga_mem_dq[49]
set_location_assignment PIN_U8 -to fpga_mem_dq[48]
set_location_assignment PIN_E1 -to fpga_mem_dq[47]
set_location_assignment PIN_E2 -to fpga_mem_dq[46]
set_location_assignment PIN_G3 -to fpga_mem_dq[45]
set_location_assignment PIN_B1 -to fpga_mem_dq[44]
set_location_assignment PIN_F3 -to fpga_mem_dq[43]
set_location_assignment PIN_D2 -to fpga_mem_dq[42]
set_location_assignment PIN_F1 -to fpga_mem_dq[41]
set_location_assignment PIN_C1 -to fpga_mem_dq[40]
set_location_assignment PIN_D4 -to fpga_mem_dq[39]
set_location_assignment PIN_A4 -to fpga_mem_dq[38]
set_location_assignment PIN_B4 -to fpga_mem_dq[37]
set_location_assignment PIN_D3 -to fpga_mem_dq[36]
set_location_assignment PIN_A3 -to fpga_mem_dq[35]
set_location_assignment PIN_A2 -to fpga_mem_dq[34]
set_location_assignment PIN_C3 -to fpga_mem_dq[33]
set_location_assignment PIN_B3 -to fpga_mem_dq[32]

# Additional I2C Interfaces
set_location_assignment PIN_F2 -to pcie_smclk
set_location_assignment PIN_G4 -to pcie_smdat

# HPS Memory
set_location_assignment PIN_D15 -to hps_mem_refclk
set_location_assignment PIN_C15 -to hps_mem_a[16]
set_location_assignment PIN_B16 -to hps_mem_a[15]
set_location_assignment PIN_B18 -to hps_mem_a[14]
set_location_assignment PIN_B19 -to hps_mem_a[13]
set_location_assignment PIN_C20 -to hps_mem_a[12]
set_location_assignment PIN_A23 -to hps_mem_a[11]
set_location_assignment PIN_A24 -to hps_mem_a[10]
set_location_assignment PIN_C21 -to hps_mem_a[9]
set_location_assignment PIN_B21 -to hps_mem_a[8]
set_location_assignment PIN_B23 -to hps_mem_a[7]
set_location_assignment PIN_B24 -to hps_mem_a[6]
set_location_assignment PIN_A26 -to hps_mem_a[5]
set_location_assignment PIN_A27 -to hps_mem_a[4]
set_location_assignment PIN_A22 -to hps_mem_a[3]
set_location_assignment PIN_A21 -to hps_mem_a[2]
set_location_assignment PIN_B25 -to hps_mem_a[1]
set_location_assignment PIN_B26 -to hps_mem_a[0]
set_location_assignment PIN_A13 -to hps_mem_act_n[0]
set_location_assignment PIN_AA18 -to hps_mem_alert_n[0]
set_location_assignment PIN_A17 -to hps_mem_ba[1]
set_location_assignment PIN_A18 -to hps_mem_ba[0]
set_location_assignment PIN_A16 -to hps_mem_bg[0]
set_location_assignment PIN_C13 -to hps_mem_ck[0]
set_location_assignment PIN_D13 -to hps_mem_ck_n[0]
set_location_assignment PIN_B14 -to hps_mem_cke[0]
set_location_assignment PIN_A12 -to hps_mem_cs_n[0]
#set_location_assignment PIN_AF23 -to hps_mem_dbi_n[4]
set_location_assignment PIN_AG9 -to hps_mem_dbi_n[3]
set_location_assignment PIN_AB20 -to hps_mem_dbi_n[2]
#set_location_assignment PIN_B8 -to hps_mem_dbi_n[1]
set_location_assignment PIN_AF23 -to hps_mem_dbi_n[1]
set_location_assignment PIN_Y21 -to hps_mem_dbi_n[0]
#set_location_assignment PIN_AD23 -to hps_mem_dq[39]
#set_location_assignment PIN_AE22 -to hps_mem_dq[38]
#set_location_assignment PIN_AA23 -to hps_mem_dq[37]
#set_location_assignment PIN_AF22 -to hps_mem_dq[36]
#set_location_assignment PIN_AC22 -to hps_mem_dq[35]
#set_location_assignment PIN_AG23 -to hps_mem_dq[34]
#set_location_assignment PIN_AA22 -to hps_mem_dq[33]
#set_location_assignment PIN_AE23 -to hps_mem_dq[32]
set_location_assignment PIN_AH16 -to hps_mem_dq[31]
set_location_assignment PIN_AG11 -to hps_mem_dq[30]
set_location_assignment PIN_AH17 -to hps_mem_dq[29]
set_location_assignment PIN_AG10 -to hps_mem_dq[28]
set_location_assignment PIN_AH15 -to hps_mem_dq[27]
set_location_assignment PIN_AH12 -to hps_mem_dq[26]
set_location_assignment PIN_AH18 -to hps_mem_dq[25]
set_location_assignment PIN_AG13 -to hps_mem_dq[24]
set_location_assignment PIN_AB21 -to hps_mem_dq[23]
set_location_assignment PIN_AH22 -to hps_mem_dq[22]
set_location_assignment PIN_AC21 -to hps_mem_dq[21]
set_location_assignment PIN_AH21 -to hps_mem_dq[20]
set_location_assignment PIN_AC20 -to hps_mem_dq[19]
set_location_assignment PIN_AG21 -to hps_mem_dq[18]
set_location_assignment PIN_AG20 -to hps_mem_dq[17]
set_location_assignment PIN_AH20 -to hps_mem_dq[16]
#set_location_assignment PIN_C12 -to hps_mem_dq[15]
#set_location_assignment PIN_C10 -to hps_mem_dq[14]
#set_location_assignment PIN_C11 -to hps_mem_dq[13]
#set_location_assignment PIN_C8  -to hps_mem_dq[12]
#set_location_assignment PIN_B9  -to hps_mem_dq[11]
#set_location_assignment PIN_D10 -to hps_mem_dq[10]
#set_location_assignment PIN_B10 -to hps_mem_dq[9]
#set_location_assignment PIN_D8  -to hps_mem_dq[8]
set_location_assignment PIN_AD23 -to hps_mem_dq[15]
set_location_assignment PIN_AE22 -to hps_mem_dq[14]
set_location_assignment PIN_AA23 -to hps_mem_dq[13]
set_location_assignment PIN_AF22 -to hps_mem_dq[12]
set_location_assignment PIN_AC22 -to hps_mem_dq[11]
set_location_assignment PIN_AG23 -to hps_mem_dq[10]
set_location_assignment PIN_AA22 -to hps_mem_dq[9]
set_location_assignment PIN_AE23 -to hps_mem_dq[8]
set_location_assignment PIN_AB19 -to hps_mem_dq[7]
set_location_assignment PIN_AB18 -to hps_mem_dq[6]
set_location_assignment PIN_Y19 -to hps_mem_dq[5]
set_location_assignment PIN_Y20 -to hps_mem_dq[4]
set_location_assignment PIN_AA19 -to hps_mem_dq[3]
set_location_assignment PIN_AA21 -to hps_mem_dq[2]
set_location_assignment PIN_W20 -to hps_mem_dq[1]
set_location_assignment PIN_W21 -to hps_mem_dq[0]
#set_location_assignment PIN_AC23 -to hps_mem_dqs[4]
#set_location_assignment PIN_AB23 -to hps_mem_dqs_n[4]
set_location_assignment PIN_AH11 -to hps_mem_dqs[3]
set_location_assignment PIN_AH10 -to hps_mem_dqs_n[3]
set_location_assignment PIN_AF21 -to hps_mem_dqs[2]
set_location_assignment PIN_AE21 -to hps_mem_dqs_n[2]
#set_location_assignment PIN_A9 -to hps_mem_dqs[1]
#set_location_assignment PIN_A8 -to hps_mem_dqs_n[1]
set_location_assignment PIN_AC23 -to hps_mem_dqs[1]
set_location_assignment PIN_AB23 -to hps_mem_dqs_n[1]
set_location_assignment PIN_AA17 -to hps_mem_dqs[0]
set_location_assignment PIN_Y17 -to hps_mem_dqs_n[0]
set_location_assignment PIN_B20 -to hps_mem_oct_rzqin
set_location_assignment PIN_A14 -to hps_mem_odt[0]
set_location_assignment PIN_E14 -to hps_mem_par[0]
set_location_assignment PIN_B11 -to hps_mem_reset_n[0]

# Displayport TX Aux
set_location_assignment PIN_Y16 -to dp_tx_hpd_in
set_location_assignment PIN_AF13 -to dp_tx_aux_p
set_location_assignment PIN_AF14 -to dp_tx_aux_n

# Displayport RX Aux
set_location_assignment PIN_T8 -to dp_rx_aux_oe
set_location_assignment PIN_V7 -to dp_rx_aux_in
set_location_assignment PIN_V6 -to dp_rx_aux_out
set_location_assignment PIN_W3 -to dp_rx_hpd_out

# HPS I2C - Includes SFP, PCIe, DP
set_location_assignment PIN_AD3 -to hps_i2c_scl
set_location_assignment PIN_AA6 -to hps_i2c_sda

# MAC Addr/ Clkgen I2C
set_location_assignment PIN_A11 -to clkgen_i2c_cl
set_location_assignment PIN_B13 -to clkgen_i2c_da

# Transceivers
set_location_assignment PIN_N24 -to dp_refclk
set_location_assignment PIN_R24 -to pcie_refclk
set_location_assignment PIN_U24 -to xge_refclk
set_location_assignment PIN_W24 -to sdi_refclk
set_location_assignment PIN_H26 -to sdi_rx_data
set_location_assignment PIN_D26 -to xge_rx_data
set_location_assignment PIN_M26 -to pcie_rx[3]
set_location_assignment PIN_P26 -to pcie_rx[2]
set_location_assignment PIN_T26 -to pcie_rx[1]
set_location_assignment PIN_V26 -to pcie_rx[0]
set_location_assignment PIN_Y26 -to dp_rx_data[3]
set_location_assignment PIN_AB26 -to dp_rx_data[2]
set_location_assignment PIN_AD26 -to dp_rx_data[1]
set_location_assignment PIN_AF26 -to dp_rx_data[0]
set_location_assignment PIN_J28 -to sdi_tx_data
set_location_assignment PIN_E28 -to xge_tx_data
set_location_assignment PIN_N28 -to pcie_tx[3]
set_location_assignment PIN_R28 -to pcie_tx[2]
set_location_assignment PIN_U28 -to pcie_tx[1]
set_location_assignment PIN_W28 -to pcie_tx[0]
set_location_assignment PIN_AA28 -to dp_tx_data[3]
set_location_assignment PIN_AC28 -to dp_tx_data[2]
set_location_assignment PIN_AE28 -to dp_tx_data[1]
set_location_assignment PIN_AG28 -to dp_tx_data[0]
set_location_assignment PIN_AB11 -to pcie_perst_n
set_location_assignment PIN_AF14 -to sdi_tx_xsd_hd
set_location_assignment PIN_AA11 -to genl_tof
set_location_assignment PIN_A19 -to genl_href
set_location_assignment PIN_D14 -to genl_vref

#******************************************************************************
# IO Standard
#******************************************************************************

set_instance_assignment -name IO_STANDARD LVDS -to fpga_mem_refclk
set_instance_assignment -name IO_STANDARD LVDS -to hps_mem_refclk
set_instance_assignment -name IO_STANDARD LVDS -to lvds_b_data_in
set_instance_assignment -name IO_STANDARD LVDS -to lvds_b_data_out
set_instance_assignment -name IO_STANDARD LVDS -to lvds_b_clk_in_int
set_instance_assignment -name IO_STANDARD LVDS -to lvds_b_clk_in_ext
set_instance_assignment -name IO_STANDARD LVDS -to lvds_b_clk_out

set_instance_assignment -name IO_STANDARD "SSTL-12" -to dp_rx_hpd_out
set_instance_assignment -name IO_STANDARD "1.2 V" -to dp_rx_aux_in
set_instance_assignment -name IO_STANDARD "SSTL-12" -to dp_rx_aux_out
set_instance_assignment -name IO_STANDARD "SSTL-12" -to dp_rx_aux_oe

set_instance_assignment -name IO_STANDARD "1.2 V" -to hps_i2c_sda
set_instance_assignment -name IO_STANDARD "1.2 V" -to hps_i2c_scl
set_instance_assignment -name IO_STANDARD "1.2 V" -to clkgen_i2c_cl
set_instance_assignment -name IO_STANDARD "1.2 V" -to clkgen_i2c_da
set_instance_assignment -name IO_STANDARD "1.2 V" -to genl_href
set_instance_assignment -name IO_STANDARD "1.2 V" -to genl_vref

set_instance_assignment -name IO_STANDARD "1.2 V" -to pcie_smclk
set_instance_assignment -name IO_STANDARD "1.2 V" -to pcie_smdat

set_instance_assignment -name IO_STANDARD LVDS -to lvds_t_data_in
set_instance_assignment -name IO_STANDARD LVDS -to lvds_t_data_out
set_instance_assignment -name IO_STANDARD LVDS -to lvds_t_clk_in_ext
set_instance_assignment -name IO_STANDARD LVDS -to lvds_t_clk_out

set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.8-V SSTL CLASS I" -to dp_tx_aux_p
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.8-V SSTL CLASS I" -to dp_tx_aux_n
#******************************************************************************
# Slew Rate
#******************************************************************************

set_instance_assignment -name SLEW_RATE 0 -to hps_i2c_sda
set_instance_assignment -name SLEW_RATE 0 -to hps_i2c_scl

#set_instance_assignment -name SLEW_RATE 0 -to dp_tx_aux_out
#set_instance_assignment -name SLEW_RATE 0 -to dp_tx_aux_oe
#set_instance_assignment -name SLEW_RATE 0 -to dp_rx_aux_out
#set_instance_assignment -name SLEW_RATE 0 -to dp_rx_aux_oe
#set_instance_assignment -name SLEW_RATE 0 -to dp_rx_hpd_out

#******************************************************************************
# Termination
#******************************************************************************

set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to lvds_b_data_in
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to lvds_b_clk_in_int
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to lvds_b_clk_in_ext
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to lvds_t_data_in
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to lvds_t_clk_in_ext
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to fpga_mem_refclk
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to hps_mem_refclk

#set_instance_assignment -name INPUT_TERMINATION "SERIES 40 OHM WITH CALIBRATION" -to dp_tx_hpd_in
#set_instance_assignment -name INPUT_TERMINATION "SERIES 40 OHM WITH CALIBRATION" -to dp_tx_aux_in
#set_instance_assignment -name INPUT_TERMINATION "SERIES 40 OHM WITH CALIBRATION" -to dp_rx_aux_in

set_instance_assignment -name OUTPUT_TERMINATION "SERIES 40 OHM WITHOUT CALIBRATION" -to dp_tx_aux_out
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 40 OHM WITHOUT CALIBRATION" -to dp_tx_aux_oe
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 40 OHM WITHOUT CALIBRATION" -to dp_rx_aux_out
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 40 OHM WITHOUT CALIBRATION" -to dp_rx_aux_oe
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 40 OHM WITHOUT CALIBRATION" -to dp_rx_hpd_out

#******************************************************************************
# Transceivers
#******************************************************************************

set_instance_assignment -name IO_STANDARD LVDS -to sdi_refclk
set_instance_assignment -name IO_STANDARD LVDS -to xge_refclk
set_instance_assignment -name IO_STANDARD LVDS -to dp_refclk
set_instance_assignment -name IO_STANDARD HCSL -to pcie_refclk

set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to sdi_refclk
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to xge_refclk
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to dp_refclk

set_instance_assignment -name XCVR_REFCLK_PIN_TERMINATION AC_COUPLING -to sdi_refclk
set_instance_assignment -name XCVR_REFCLK_PIN_TERMINATION AC_COUPLING -to xge_refclk
set_instance_assignment -name XCVR_REFCLK_PIN_TERMINATION AC_COUPLING -to dp_refclk

set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 15 -to sdi_tx_data
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_NEG -to sdi_tx_data

set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 19 -to dp_tx_data
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_NEG -to dp_tx_data

set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to sdi_rx_data
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to sdi_tx_data
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to xge_rx_data
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to xge_tx_data
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_tx_data
set_instance_assignment -name IO_STANDARD CML -to pcie_rx
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to pcie_tx

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to pcie_rx
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to pcie_tx
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to dp_rx_data
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to dp_tx_data
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to sdi_rx_data
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to sdi_tx_data
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to xge_rx_data
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to xge_tx_data

set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to sdi_rx_data
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to xge_rx_data
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to dp_rx_data
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to pcie_rx

set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to sdi_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_1 -to sdi_rx_data
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM STG1_GAIN7 -to sdi_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_15 -to sdi_rx_data

set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE S1_MODE -to xge_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_1 -to xge_rx_data
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM STG1_GAIN7 -to xge_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_15 -to xge_rx_data

set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to dp_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_1 -to dp_rx_data
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM STG1_GAIN7 -to dp_rx_data
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_15 -to dp_rx_data

set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to pcie_rx
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_2 -to pcie_rx
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to pcie_rx
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_18 -to pcie_rx

set_global_assignment -name VERILOG_INPUT_VERSION SYSTEMVERILOG_2005
set_global_assignment -name VERILOG_SHOW_LMF_MAPPING_MESSAGES OFF
set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top