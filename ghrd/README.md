# Golden Hardware Reference Design

## Contents

| File                                        | Description
| ------------------------------------------- | ------------------------------------------------------
| compile_all.tcl                             | Compiles the design for all FPGA types.
| dreamchip_arria10som_base_board_info.xml    | Baseboard specific addition to the Linux device tree.
| dreamchip_arria10som_common_board_info.xml  | Arria 10 specific additions to the Linux device tree.
| ghrd.qpf                                    | Quartus project file.
| ghrd.qsf                                    | Quartus setting file.
| ghrd.sdc                                    | Default clock constraints.
| ghrd.v                                      | Verilog top level.
| hps_isw_handoff                             | Handoff folder for HPS Uboot generation.
| hps.qsys                                    | QSYS system with HPS, DDR4 controller and periphery.
| hps.sopcinfo                                | Required for Linux device tree generation. 
| Makefile                                    | Makefile to compile the design.
| release_notes.txt                           | Releasse notes/Version Information

The Golden Hardware Reference Designs feature the HPS system including its DDR4 memory controller. Two variants are available, depending on primary boot medium. These are also the reference designs required to generate the FPGA bitstreams for the supoported Uboot/Linux boot flow in this repository.

While most pins of the SOM are not actively used in the GHRD, the Quartus project includes all pin locations and constraints used to realize the interfaces on the reference baseboard. It is therefore an ideal starting point for new customer designs.

## SD-Card Design (ghrd_sdcard)

The ghrd_sdcard design is used when an SD-Card on the base board is used as a primary boot flash. It requires a version of the Board Management Controller that configures the BSEL pins of the HPS for 1.8V SD-Card boot, and the FPGA MSEL pins to HPS boot. Please note that the SD-Card cannot be used at the same time as the on-SOM eMMC, because they share a controller.

## QSPI/eMMC Design (ghrd_qspi)

The ghrd_qspi design is used when the QSPI NOR Flash on the SOM is used as a primary boot flash, usually with the on-SOM eMMC as a secondary boot flash. It requires a version of the Board Management Controller that configures the BSEL pins of the HPS for 1.8V QSPI boot, and the FPGA MSEL pins to HPS boot. Please note that the SD-Card cannot be used at the same time as the on-SOM eMMC, because they share a controller.
