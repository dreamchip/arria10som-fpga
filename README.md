# Introduction

This repository features the reference designs for the Dreamchip Arria 10 SOM variants. The designs can be used as a starting point for customer designs using the various interfaces found on the SOM and the reference baseboards. Most designs are based on the Quartus/QSYS example design with adaptions to the FPGA sizes and pinning used on the SOM.

# Reference Designs

Unless otherwise specified, all Quartus projects use Quartus Prime Standard Edition Version 17.1.590.

Each interface specific design includes a compile_all.tcl script for batch compilation of all available FPGA sizes on the SOM. If only a specific variant is needed, the project can manually be updated using the specified FPGA type. 
