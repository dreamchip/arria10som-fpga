# PCIe (pcie_test)

### Contents

| File               |  Description
| ------------------ | -------------------------------------------------
| compile_all.tcl    | Compiles the design for all FPGA types.
| Makefile           | Makefile to compile the design.
| pcie_x4.qsys       | QSYS system of PCIE hard IP.
| pcie_test.qpf      | Quartus project file.
| pcie_test.qsf      | Quartus setting file.
| pcie_test.sdc      | Default clock constraints.
| pcie_test.v        | Verilog top level.

[Intel® Arria® 10 Avalon-MM DMA Interface for PCIe* Solutions User Guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/archives/ug_a10_pcie_avmm_dma-17-0.pdf)

The modified example design contains a PCIe Gen3 x4 interface some SRAm for performance testing. please refer to the user guide for the associated Windows/Linux software to perform these tests.
