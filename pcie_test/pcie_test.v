//-----------------------------------------------------------------------------
// $HeadURL$
// $Author$
// $Rev$
// $Date$
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in
// DreamChip Technologies GmbH. The information contained herein is the property
// of DreamChip Technologies GmbH and is supplied without liability for errors or
// omissions. No part may be reproduced or used except as authorized by
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2017. All rights reserved.
//-----------------------------------------------------------------------------
// Module    : ghrd.v
// Hierarchy : Top Level
// Purpose   : Golden Hardware Reference Design for DCT Arria 10 SoM
// Creator   : Eric Bunker
// Remark    : v0.4
//-----------------------------------------------------------------------------

module pcie_test (  
  // PCIe Gen3 x4 ------------------------------------------------------  
  input  wire          lvds_b_clk_in_int,
  input  wire          pcie_perst_n,
  input  wire          pcie_refclk,
  input  wire [3:0]    pcie_rx,
  output wire [3:0]    pcie_tx
);

  reg [63:0] clk_counter = 64'b0 /* synthesis noprune */;

  pcie_x4 pcie_x4_inst (
    .hip_ctrl_test_in        (32'h188),      // hip_ctrl.test_in
    .hip_ctrl_simu_mode_pipe (1'b0),         //         .simu_mode_pipe
    .npor_npor               (pcie_perst_n), //     npor.npor
    .npor_pin_perst          (pcie_perst_n), //         .pin_perst
    .refclk_clk              (pcie_refclk),  //   refclk.clk
    .serial_rx_in0           (pcie_rx[0]),   //   serial.rx_in0
    .serial_rx_in1           (pcie_rx[1]),   //         .rx_in1
    .serial_rx_in2           (pcie_rx[2]),   //         .rx_in2
    .serial_rx_in3           (pcie_rx[3]),   //         .rx_in3
    .serial_tx_out0          (pcie_tx[0]),   //         .tx_out0
    .serial_tx_out1          (pcie_tx[1]),   //         .tx_out1
    .serial_tx_out2          (pcie_tx[2]),   //         .tx_out2
    .serial_tx_out3          (pcie_tx[3])    //         .tx_out3
  );
  
  always @ (posedge pcie_refclk) begin
    clk_counter <= clk_counter + 1;
  end

endmodule
