# Bitstreams

Bitstreams for all reference designs and supported FPGA device types have been generated for reference. 

## Arria 10 Bitstream File Name

| File Name                                   | Description
| ------------------------------------------- | ----------------------
| <design_name>_10AS016E4.sof                 | 160 kLE FPGA.
| <design_name>_10AS022E4.sof                 | 220 kLE FPGA.
| <design_name>_10AS027E4.sof                 | 270 kLE FPGA.
| <design_name>_10AS032E4.sof                 | 320 kLE FPGA.
| <design_name>_10AS048E4.sof                 | 480 kLE FPGA.

## Power Sequencer File Name

| File Name                       | HPS Boot Source | FPGA Boot Source
| --------------------------------| --------------- | ----------------
│ power_sequencer_bsel0_msel0.pof | Auto detect (SD/QSPI) | HPS Config
│ power_sequencer_bsel0_msel1.pof | Auto detect (SD/QSPI) | QSPI Config
│ power_sequencer_bsel2_msel0.pof | SD                    | HPS Config 
│ power_sequencer_bsel2_msel1.pof | SD                    | QSPI Config
│ power_sequencer_bsel3_msel0.pof | QSPI                  | HPS Config 
│ power_sequencer_bsel3_msel1.pof | QSPI                  | QSPI Config

The default clocks are as follows:

| Output | IO Standard | Default Frequency | User Adjustable | Default Baseboard Function
| ------ | ----------- | ----------------- | --------------- | --------------------------
| 0      | LVDS        | 135 MHz           | Yes             | Displayport refclk
| 1      | N/A         | N/A               | Yes             | Baseboard refclk 0
| 2      | LVDS        | 100 MHz           | Yes             | Baseboard refclk 1
| 3      | LVDS        | 297 MHz           | Yes             | Baseboard refclk 2
| 4      | LVDS        | 233.333 MHz       | Yes             | FPGA memory refclk
| 5      | LVDS        | 233.333 MHz       | Yes             | HPS memory refclk
| 6      | S/E         | 100 MHz           | No              | CLK_USR/config clk
| 7      | LVDS        | 100 MHz           | Yes             | LVDS bottom refclk
| 8      | LVDS        | N/A               | No              | Unused
| 9      | LVDS        | 322.265625 MHz    | Yes             | 10G Ethernet refclk

If you require different clocks, please refer to the power sequencer readme to generate a new configuration.
